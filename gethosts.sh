#!/bin/bash
#cat Vagrantfile |grep ip |awk '{ print $4, $1 }' | tr -d \"  |sed 's/.vm.network//'
#this file takes a vagrant file and create a host file to copied over to linux vms

cat ./Vagrantfile | \
    grep ip | \
    awk '{ print $4, $1 }' | \
    tr -d \" | \
    sed 's/.vm.network//' >  hosts_file_for_vms
    

