1 jumpoff, 3 managers 3 workers

install ansible on jumpoff machine via vagrant init playbook
copy of hosts file 
    (cat Vagrantfile |grep ip |awk '{ print $4, $1 }' | tr -d \"  |sed 's/.vm.network//')
                    ./gethosts.sh
log in via vagrant ssh to jumpoff
./key/id_rsa and id_rsa.pub
create a local user on all machines, generate a key from local to all machines
add private key on all machines

ansible-playbook -i 192.168.56.201, install_ansible_on_jumpoff.yml --key-file ./keys/id_rsa -u chris
ansible-playbook -i ./AnsibleHosts software_environment1.yml --key-file ./keys/id_rsa -u chris